package pl.pmajewski.flashcat.gateway.client;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import pl.pmajewski.flashcat.gateway.dto.JWTParameters;

@FeignClient(name = "account-service")
public interface AccountClient {
	
	@GetMapping("/jwt/settings")
	@Cacheable(cacheNames = "jwtSettingsCache")
	JWTParameters getJWTParamters();
}
