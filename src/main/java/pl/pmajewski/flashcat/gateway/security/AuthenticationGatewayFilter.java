package pl.pmajewski.flashcat.gateway.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import pl.pmajewski.flashcat.gateway.client.AccountClient;
import pl.pmajewski.flashcat.gateway.dto.JWTParameters;
import pl.pmajewski.flashcat.gateway.exception.InvalidTokenAutorization;

@Component
public class AuthenticationGatewayFilter extends AbstractGatewayFilterFactory  {
	
	@Autowired
	private AccountClient client;
	
	@Override
	public GatewayFilter apply(Object config) {
		return (exchange, chain) -> {
			ServerHttpRequest request = exchange.getRequest();
			if(request.getPath().toString().startsWith("/api")) {
				Long userId = extractUser(request.getHeaders().get("Authorization"));
				exchange.getRequest()
					.mutate()
                	.header("userId", userId.toString())
                	.build();
				
				return chain.filter(exchange.mutate().request(request).build());
			} else {
				return chain.filter(exchange);
			}
		};
	}
	
	private Long extractUser(List<String> data) {
		if(data == null || data.isEmpty()) {
			throw new InvalidTokenAutorization("Token required");
		}

		JWTParameters jwtParams = client.getJWTParamters();
		String authToken = data.get(0);

		try {
			String username = Jwts.parser()
					.setSigningKey(jwtParams.token)
					.parseClaimsJws(authToken)
					.getBody()
					.getSubject();
			return Long.parseLong(username);
		} catch (Exception e) {
			throw new InvalidTokenAutorization("Invalid Token");
		}
	}
}
