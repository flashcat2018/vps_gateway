package pl.pmajewski.flashcat.gateway.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class InvalidTokenAutorization extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvalidTokenAutorization() {
		super();
	}

	public InvalidTokenAutorization(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public InvalidTokenAutorization(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public InvalidTokenAutorization(String arg0) {
		super(arg0);
	}

	public InvalidTokenAutorization(Throwable arg0) {
		super(arg0);
	}
}
