package pl.pmajewski.flashcat.gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CachingSupervisor {

	@Autowired
	private CacheManager cacheManager;

	@Scheduled(fixedRate = 600000) // 10 min
	public void cleanCache() {
		cacheManager.getCacheNames().parallelStream().forEach(name -> cacheManager.getCache(name).clear());
	}
}
