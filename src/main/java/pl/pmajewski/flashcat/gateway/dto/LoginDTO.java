package pl.pmajewski.flashcat.gateway.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginDTO {

	public Long id;
	public String username;
	public LocalDateTime registerTime;
	public String token;
}
