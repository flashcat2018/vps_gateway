package pl.pmajewski.flashcat.gateway.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
@NoArgsConstructor
public class JWTParameters {

	public String token;
	public Long lifetime;
}
